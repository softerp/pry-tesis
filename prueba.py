import tkinter as tk

class card_game_gui:

    def __init__(self, root):
        self.pagenum = 1
        self.root = root

        #<-- configure color of your master window
        self.root.configure(background='blue')

    def page1(self):
        page = tk.Frame(self.root, bg="blue") #<-- this line has seemingly no effect
        form = tk.Frame(page, bg="white")
        self.root.grid_columnconfigure(0, weight=1)

        tk.Label(self.root, text='Add player', bg="dark gray").grid(row=0, column=0, columnspan=2, sticky="NESW")

        self.user = tk.Entry(form, bd=0, bg="light gray")
        self. user.bind("<FocusIn>", self.UserEntryFocusIn)
        self.user.bind("<FocusOut>", self.UserEntryFocusOut)
        self.user.grid(row=1, column=1, sticky="NSEW", pady=5)

        user_label = tk.Label(form, text="USERNAME:", bg="white")
        user_label.grid(row=1, column=0, sticky="NSEW")

        self.pw = tk.Entry(form, bd=0, bg="light gray", show="*")
        self.pw.bind("<FocusIn>", self.PwEntryFocusIn)
        self.pw.bind("<FocusOut>", self.PwEntryFocusOut)
        self.pw.grid(row=2, column=1, sticky="EW", pady=5)

        pw_label = tk.Label(form, text="PASSWORD:", bg="white")
        pw_label.grid(row=2, column=0, sticky="NSEW")

        tk.Button(form, text='To page 2', command=self.changepage).grid(row=3, column=0)

        page.grid(pady=50, padx=50)
        form.grid()

    def UserEntryFocusIn(self, event):
        self.user.config(bg="white")

    def UserEntryFocusOut(self, event):
        self.user.config(bg="light gray")

    def PwEntryFocusIn(self, event):
        self.pw.config(bg="white")

    def PwEntryFocusOut(self, event):
        self.pw.config(bg="light gray")

    def page2(self):
        root = self.root

        page = tk.Frame(root, bg='blue')
        page.grid()
        tk.Label(page, text = 'This is page 2').grid(row = 0)
        tk.Button(page, text = 'To page 1', command = self.changepage).grid(row = 1)

    def changepage(self):
        root = self.root

        for widget in root.winfo_children():
            widget.destroy()
        if self.pagenum == 1:
            self.page2()
            self.pagenum = 2
        else:
            self.page1()
            self.pagenum = 1

root = tk.Tk()
card_game_gui = card_game_gui(root)
card_game_gui.page1()
root.mainloop()