#calculo del indice de infestacion y daño
import cv2
import numpy as np
from PIL import Image, ImageFilter
#caso Roya de café IID(Indice de Intensidad de Daño)
#conteo de pixeles region sana



imagen_sana = cv2.imread("sana_segmentado.png")
imagen_enferma = cv2.imread("enfermedad_segmentado.png")
imagen_sana_ = cv2.resize(imagen_sana,(200,200), interpolation = cv2.INTER_AREA)
imagen_enferma_ = cv2.resize(imagen_enferma,(200,200), interpolation = cv2.INTER_AREA)

def contar_pixeles_no_blancos(img):#img region sana y enferma 
	rows = len(img)
	cols = len(img[0])
	#print(rows,cols)
	#print('-------------------------')
	cantidad_region_sana=0
	for k in range(0,3):
		for i in range(rows):
		    for j in range(cols):
		        
		        if(img[i,j,k]!=255):# pixeles diferentes al color blanco correspondiente a la region sana
		        	cantidad_region_sana=cantidad_region_sana+1

	return cantidad_region_sana




#print(pixeles_sanos,pixeles_enfermos)

def grado_severidad_hoja():
	pixeles_sanos=contar_pixeles_no_blancos(imagen_sana_)#numero de pixeles sanos
	pixeles_enfermos=contar_pixeles_no_blancos(imagen_enferma_)# numero de pixeles enfermos
	total=pixeles_enfermos+pixeles_sanos
	severidad=round((pixeles_enfermos/total),4)
	#grado=(pixeles_enfermos/total)
	mensaje=""
	if(severidad==0):
		mensaje="HOJA SANA, PERTENECE AL GRADO 0:",severidad
	if(severidad>=0.01 and severidad<=0.05):
		mensaje="GRADO 1:", severidad
	if(severidad>0.05 and severidad<=0.2):
		mensaje="GRADO 2:", severidad
	if(severidad>0.2 and severidad<=0.5):
		mensaje="GRADO 3:", severidad
	if(severidad>0.5):
		mensaje="GRADO 4:",severidad
	return mensaje


#print(x)
