
#HISTOGRAMA
import cv2
import numpy as np
from matplotlib import pyplot as plt

import matplotlib.pyplot as plt
import matplotlib.image as mpimg

def histograma_grises(ruta_imagen):
	gray_img = cv2.imread(ruta_imagen, cv2.IMREAD_GRAYSCALE)
	cv2.imshow('GoldenGate',gray_img)
	hist = cv2.calcHist([gray_img],[0],None,[256],[0,256])
	plt.hist(gray_img.ravel(),256,[0,256])
	plt.title('Histograma escala de grises')
	plt.show()
	while True:
	    k = cv2.waitKey(0) & 0xFF     
	    if k == 27: break             # ESC key to exit 
	cv2.destroyAllWindows()

def histograma_color(ruta_imagen):
	img = cv2.imread(ruta_imagen, -1)
	cv2.imshow('GoldenGate',img)

	color = ('b','g','r')
	for channel,col in enumerate(color):
	    histr = cv2.calcHist([img],[channel],None,[256],[0,256])
	    plt.plot(histr,color = col)
	    plt.xlim([0,256])
	plt.title('Histogram for color scale picture')
	plt.show()

	while True:
	    k = cv2.waitKey(0) & 0xFF     
	    if k == 27: break             # ESC key to exit 
	cv2.destroyAllWindows()

#histograma_grises()

#----------------------------------
# CASO CLASIFICACION DE IMAGENES
#----------------------------------
#1) Ecualizacion de una imagen a color
def EcualizacionHistograma(ruta_img):
	img = cv2.imread(ruta_img)
	# Convert it to grayscale 
	img_gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
	#cv2.imshow('Input grayscale image', img_gray)
	# Equalize the histogram
	img_gray_histeq = cv2.equalizeHist(img_gray) 
	#cv2.imshow('Histogram equalized - grayscale', img_gray_histeq)
	#ECUALIZADO DE IMAGEN DE COLOR
	# Histogram equalization of color images 
	img_yuv = cv2.cvtColor(img, cv2.COLOR_BGR2YUV) 
	img_yuv[:,:,0] = cv2.equalizeHist(img_yuv[:,:,0])
	img_histeq = cv2.cvtColor(img_yuv, cv2.COLOR_YUV2BGR)
	#mostrar imagenes
	img_ = cv2.resize(img, (250,250), interpolation = cv2.INTER_AREA)
	img_histeq_ = cv2.resize(img_histeq, (250,250), interpolation = cv2.INTER_AREA)
	return  img_,img_histeq_

#x,y=EcualizacionHistograma('IMG_2256.JPG')
#cv2.imshow('Original', x) 
#cv2.imshow('Ecualizacion del hisgrama - color', y) 
#cv2.waitKey()
#------------------------------------------------------
# CASO SEGMENTACION DE IMAGENES
#------------------------------------------------------
#2) Filtro gausseano
from PIL import Image, ImageFilter
def filtro_gausseano(ruta_img):
	imagen_original = Image.open(ruta_img)
	tamaño = (5,5)
	coeficientes = [0, 1, 2, 1, 1, 3, 5, 3, 1, 2, 5, 9, 5, 2, 1, 3, 5, 3, 1, 0, 1, 2, 1, 0]
	'''
	en caso de aplicar el filtro con kernel 3x3:
	tamaño = (3,3)
	coeficientes = [1, 2, 1, 2, 4, 2, 1, 2, 1]
	'''
	imagen_filtrada = imagen_original.filter(ImageFilter.Kernel(tamaño, coeficientes))
	imagen_original_ = imagen_original.resize((350,350), Image.ANTIALIAS)
	imagen_suavizado = imagen_filtrada.resize((350,350), Image.ANTIALIAS)

	imagen_original_.save('img_preprocesado/filtro_gauss_original.JPG')
	imagen_suavizado.save('img_preprocesado/filtro_gauss.JPG')
	#se cierran ambos objetos creados de la clase Image
	imagen_original.close()
	imagen_procesada.close()
#filtro_gausseano('IMG_2256.JPG')
# 2.1 filtro gausseano 
def filtro_gauss_blur(ruta_img,desviacion):#DESVIACION = 3
	imagen_original = Image.open(ruta_img)
	imagen_filtrada = imagen_original.filter(ImageFilter.GaussianBlur(desviacion)) 

	imagen_original_ = imagen_original.resize((250,250), Image.ANTIALIAS)
	imagen_filtrada_ = imagen_filtrada.resize((250,250), Image.ANTIALIAS)

	imagen_filtrada_.save('img_preprocesado/img_gauss_blur.JPG')
	imagen_original_.save('img_preprocesado/img_original.JPG')
	imagen_original_.close()
	imagen_filtrada.close()

filtro_gauss_blur('IMG_2256.JPG',27)
#3) Filtro de Mediana

def filtro_mediana(ruta_img):
	image = cv2.imread(ruta_img)
	img_me = cv2.resize(image,(250,250), interpolation = cv2.INTER_AREA)
	# apply the 3x3 median filter on the image
	processed_image = cv2.medianBlur(img_me,3)
	#kernel = np.ones((3,3),np.float32)/9
	#processed_image = cv2.filter2D(image,-1,kernel)
	# save image to disk
	img_ = cv2.resize(image,(250,250), interpolation = cv2.INTER_AREA)
	img_mediana = cv2.resize(processed_image, (250,250), interpolation = cv2.INTER_AREA)
	#cv2.imwrite('processed_image.png', img_)
	# pause the execution of the script until a key on the keyboard is pressed
	return img_, img_mediana

'''
x,y=filtro_mediana('IMG_2256.JPG')
cv2.imshow('Original', x) 
cv2.imshow('Mediana - color', y) 
cv2.waitKey()

'''
ruta='img_preprocesado/img_gauss_blur.JPG'
original='img_preprocesado/img_original.JPG'
x=cv2.imread(ruta)
y=cv2.imread(original)
cv2.imshow('GaussianBlur', x) 
cv2.imshow('Original', y) 
cv2.waitKey()

'''
def img_preprocesado(ruta_img):
	x,y=EcualizacionHistograma(ruta_img)
	suavizado('ecualizado.jpg')

img_preprocesado(ruta)

'''