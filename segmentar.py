import cv2
import numpy as np

## Read
#img = cv2.imread('ima.png')   

def segmentacion_enfermedad(img):
    ## convert to hsv
    hsv = cv2.cvtColor(img, cv2.COLOR_BGR2HSV)
    #mask of green (36,25,25) ~ (86, 255,255)
    mask = cv2.inRange(hsv, (36, 25, 25), (86, 255,255))
    ## slice the green

    #mask_ = cv2.bitwise_xor(mask, maskmod)
    #imask = mask<255
    imask = mask<200
    #enfermedad = np.zeros_like(img, np.float32)
    enfermedad = 255*np.ones_like(img)
    enfermedad[imask] = img[imask]
    cv2.imwrite("enfermedad_segmentado.png", enfermedad)

def segmentacion_sana(img):
    ## convert to hsv
    hsv = cv2.cvtColor(img, cv2.COLOR_BGR2HSV)
    #mask of green (36,25,25) ~ (86, 255,255)
    mask = cv2.inRange(hsv, (36, 25, 25), (86, 255,255))
    #mask = cv2.inRange(hsv, (36, 25, 25), (70, 255,255))
    ## slice the green
    imask = mask>0
    #enfermedad = np.zeros_like(img, np.uint8)
    enfermedad = 255*np.ones_like(img)
    enfermedad[imask] = img[imask]
    ## save 
    cv2.imwrite("sana_segmentado.png", enfermedad)


#segmentacion_color(cv2.imread('ima.png'))

#---------------------------------

