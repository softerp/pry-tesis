from tkinter import *
import cv2
from PIL import ImageTk, Image
from tkinter import filedialog
import os

#from preprocesamiento import img_preprocesado,histograma_color,histograma_grises
from segmentar import segmentacion_enfermedad,segmentacion_sana
from informacion_tecnica import Informacion_tecnica_por_enfermedad,mostrar_nombre_comun
from IID_III import grado_severidad_hoja
from predict import predecir, predecirVGG16,predecirInceptionV3,predecirAlexNet

global c1,c2,c3,c4
global frame1,frame2,frame3,frame4,frame5,frame6,frame7,frame8,frame9
#MI VENTANA PRINCIPAL
titulo="Prototipo de un sistema de clasificación de enfermedades en las hojas del cafeto basado en visión computacional"
raiz = Tk()
raiz.geometry("800x800")
raiz.title(titulo)


#-----------------------------
#INTERCAFE CONTENEDOR
#-----------------------------

frame1=Frame(raiz, width=200, height=200)
frame1.grid(row=0, column=0)

frame2=Frame(raiz, width=200, height=200)
frame2.grid(row=1, column=0)

frame3=Frame(raiz, width=200, height=200)#libre
frame3.grid(row=2, column=0)#libre


frame4=Frame(raiz, width=300, height=300)
frame4.grid(row=0, column=1)

frame5=Frame(raiz, width=300, height=300)
frame5.grid(row=1, column=1)

frame6=Frame(raiz, width=300, height=300)#libre
frame6.grid(row=2, column=1)#libre


frame7=Frame(raiz, width=200, height=200)
frame7.grid(row=0, column=3)

frame8=Frame(raiz, width=200, height=200)
frame8.grid(row=1, column=3)

frame9=Frame(raiz, width=200, height=200)
frame9.grid(row=2, column=3)
# Informacion por cada frame
#columna 2 informacion tecnica
LABEL=Label(frame4,text="INFORMACIÓN TÉCNICA",fg="blue").pack()
#frame columna 3
LABEL=Label(frame7, text="Región  sana y enfermeda").pack()
LABEL=Label(frame8, text="Grado de Severidad").pack()

#LABEL=Label(frame9, text="Indice de infestacion o indice de intensidad de daño").pack()
#1) MODULOS CARGAR IMAGEN
def abrir_fichero():
    fichero = filedialog.askopenfilename(title='open')
    return fichero

def abrir_imagen():
    global x
    global panel
    x = abrir_fichero()
    img = Image.open(x)
    img = img.resize((150, 150), Image.ANTIALIAS)
    img = ImageTk.PhotoImage(img)
    panel = Label(frame1, image=img)
    panel.image = img
    panel.pack()
#2) PREPROCESAR IMAGEN
def preprocesar_imagen():
    #print(x)
    global imagen_mejorado
    imagen_mejorado=img_preprocesado(x)
    print(cv2.imread(x))
    #cv2.imwrite("preprocesado.png", imagen_mejorado)
    #histograma_color(x)
    #histograma_grises(x)
#3) SEGMENTAR IMAGEN
def segmentar_imagen():
    #print(x) 
    #segmentacion_enfermedad(cv2.imread('preprocesado.png'))
    #segmentacion_sana(cv2.imread('preprocesado.png'))

    segmentacion_enfermedad(cv2.imread(x))
    segmentacion_sana(cv2.imread(x))

    img_e=Image.open('enfermedad_segmentado.png')
    img_s=Image.open('sana_segmentado.png')
    print(cv2.imread('sana_segmentado.png'))

    img_e = img_e.resize((150, 150), Image.ANTIALIAS)
    img_s = img_s.resize((150, 150), Image.ANTIALIAS)

    img_e = ImageTk.PhotoImage(img_e)
    img_s = ImageTk.PhotoImage(img_s)

    panel = Label(frame7, image=img_e)
    panels = Label(frame7, image=img_s)

    panel.image = img_e
    panels.image = img_s

    panels.pack()
    panel.pack()

#4) CLASIFICAR IMAGEN

#c_enfermedad='Hemileia Vastatrix'#AQUI PREDECIR ENFERMEDAD DE LA IMAGEN 
# caso propio CNN
def clase_enfermedad():
    global c_enfermedad
    global Respuesta
    global porcentaje_similitud
    c_enfermedad,Respuesta,porcentaje_similitud=predecir(x)
    print(c_enfermedad,Respuesta,porcentaje_similitud)
    nombre_comun=mostrar_nombre_comun(Respuesta)
  

    L_clase=Label(frame2,text="Nombre Cientifico", relief=RIDGE,  width=20).pack()
    text = Text(frame2,width=25, height=2)
    text.insert(INSERT,Respuesta)
    text.pack()
    L_clase=Label(frame2,text="Nombre Cómun", relief=RIDGE,  width=20).pack()
    text = Text(frame2,width=25, height=2)
    text.insert(INSERT,nombre_comun)
    text.pack()

#CASO VGG-16
def clase_enfermedadVGG16():
    global c_enfermedad
    global Respuesta
    global porcentaje_similitud
    c_enfermedad,Respuesta,porcentaje_similitud=predecirVGG16(x)
    print(c_enfermedad,Respuesta,porcentaje_similitud)
    nombre_comun=mostrar_nombre_comun(Respuesta)
  

    L_clase=Label(frame2,text="Nombre Cientifico", relief=RIDGE,  width=20).pack()
    text = Text(frame2,width=25, height=2)
    text.insert(INSERT,Respuesta)
    text.pack()
    L_clase=Label(frame2,text="Nombre Cómun", relief=RIDGE,  width=20).pack()
    text = Text(frame2,width=25, height=2)
    text.insert(INSERT,nombre_comun)
    text.pack()

#CASO AlexNet
def clase_enfermedadAlexNet():
    global c_enfermedad
    global Respuesta
    global porcentaje_similitud
    c_enfermedad,Respuesta,porcentaje_similitud=predecirAlexNet(x)
    print(c_enfermedad,Respuesta,porcentaje_similitud)
    nombre_comun=mostrar_nombre_comun(Respuesta)
  

    L_clase=Label(frame2,text="Nombre Cientifico", relief=RIDGE,  width=20).pack()
    text = Text(frame2,width=25, height=2)
    text.insert(INSERT,Respuesta)
    text.pack()
    L_clase=Label(frame2,text="Nombre Cómun", relief=RIDGE,  width=20).pack()
    text = Text(frame2,width=25, height=2)
    text.insert(INSERT,nombre_comun)
    text.pack()

#CASO INCEPTION V3
def clase_enfermedadinceptionv3():
    global c_enfermedad
    global Respuesta
    global porcentaje_similitud
    c_enfermedad,Respuesta,porcentaje_similitud=predecirInceptionV3(x)
    print(c_enfermedad,Respuesta,porcentaje_similitud)
    nombre_comun=mostrar_nombre_comun(Respuesta)
  

    L_clase=Label(frame2,text="Nombre Cientifico", relief=RIDGE,  width=20).pack()
    text = Text(frame2,width=25, height=2)
    text.insert(INSERT,Respuesta)
    text.pack()
    L_clase=Label(frame2,text="Nombre Cómun", relief=RIDGE,  width=20).pack()
    text = Text(frame2,width=25, height=2)
    text.insert(INSERT,nombre_comun)
    text.pack()

#5) INFORMACION TECNICA

def informacion_tecnica():
    #print(Respuesta)
    Informacion_tecnica=Informacion_tecnica_por_enfermedad(Respuesta)
    #forma de mostrar la informaciom
    L_biologia=Label(frame4,text="Biología", relief=RIDGE,  width=20).pack()
    text = Text(frame4,width=30, height=8)
    text.insert(INSERT, Informacion_tecnica[0])
    text.pack()

    L_Agente=Label(frame4,text="Agente", relief=RIDGE,  width=20).pack()
    text = Text(frame4,width=30, height=2)
    text.insert(INSERT, Informacion_tecnica[1])
    text.pack()

    L_Danio=Label(frame5,text="Daño", relief=RIDGE,  width=20).pack()
    text = Text(frame5,width=30, height=8)
    text.insert(INSERT, Informacion_tecnica[2])
    text.pack()

    L_Control=Label(frame5,text="Control", relief=RIDGE,  width=20).pack()
    text = Text(frame5,width=30, height=8)
    text.insert(INSERT, Informacion_tecnica[3])
    text.pack()
#6) INDICE DE INTENSIDAD Y SEVERIDAD

def grado_severidad_hoja_Roya():
   
    L_clase=Label(frame8,text="Grado Severidad Roya", relief=RIDGE,  width=20).pack()
    text = Text(frame8,width=25, height=2)
    #x=grado_severidad_hoja()
    grado= grado_severidad_hoja()
    text.insert(INSERT,grado)
    text.pack()


#7) procesar imagen
def procesar_imagen():
    #preprocesar_imagen()
    L_clase=Label(frame7,text="Región sana y enferma", relief=RIDGE,  width=20).pack()
    text = Text(frame7,width=25, height=2)
    clase_enfermedad()
    informacion_tecnica()
    if(Respuesta=="Hemileia Vastatrix"):
        segmentar_imagen()
        grado_severidad_hoja_Roya()
def procesar_imagenVGG16():
    #preprocesar_imagen()
    L_clase=Label(frame7,text="Región sana y enferma", relief=RIDGE,  width=20).pack()
    text = Text(frame7,width=25, height=2)
    clase_enfermedadVGG16()
    informacion_tecnica()
    if(Respuesta=="Hemileia Vastatrix"):
        segmentar_imagen()
        grado_severidad_hoja_Roya()

def procesar_imageninceptionv3():
    #preprocesar_imagen()
    L_clase=Label(frame7,text="Región sana y enferma", relief=RIDGE,  width=20).pack()
    text = Text(frame7,width=25, height=2)
    clase_enfermedadinceptionv3()
    informacion_tecnica()
    if(Respuesta=="Hemileia Vastatrix"):
        segmentar_imagen()
        grado_severidad_hoja_Roya()

def procesar_imagenAlexNet():
    #preprocesar_imagen()
    L_clase=Label(frame7,text="Región sana y enferma", relief=RIDGE,  width=20).pack()
    text = Text(frame7,width=25, height=2)
    clase_enfermedadAlexNet()
    informacion_tecnica()
    if(Respuesta=="Hemileia Vastatrix"):
        segmentar_imagen()
        grado_severidad_hoja_Roya()

def borrarPantalla(): #Definimos la función estableciendo el nombre que queramos
    
    #panel.destroy()
    for widget in frame1.winfo_children():
        widget.destroy()
    for widget in frame2.winfo_children():
        widget.destroy()
    for widget in frame3.winfo_children():
        widget.destroy()
    for widget in frame4.winfo_children():
        widget.destroy()
    for widget in frame5.winfo_children():
        widget.destroy()
    for widget in frame6.winfo_children():
        widget.destroy()
    for widget in frame7.winfo_children():
        widget.destroy()
    for widget in frame8.winfo_children():
        widget.destroy()
    ejecutar()

def ejecutar():
    btn1 = Button(frame1, text='carga imagen a procesar', command=abrir_imagen,bg="dark gray").pack()
    #btn = Button(frame1, text='Preprocesar imagen', command=preprocesar_imagen).pack()
    #btn2= Button(frame1, text='Segmentar imagen', command=segmentar_imagen).pack()
    #btn3= Button(frame1, text='Informacion tecnica', command=informacion_tecnica).pack()
    #btn4= Button(frame1, text='clase enfermedad', command=clase_enfermedad).pack()

    c1 = Checkbutton(frame1, text='CNN propio', onvalue=1, offvalue=0, command=procesar_imagen).pack()
    c2 = Checkbutton(frame1, text='CNN VGG-16', onvalue=1, offvalue=0, command=procesar_imagenVGG16).pack()
    c3 = Checkbutton(frame1, text='CNN AlexNet', onvalue=1, offvalue=0, command=procesar_imagenAlexNet).pack()
    c4 = Checkbutton(frame1, text='Inception V3', onvalue=1, offvalue=0, command=procesar_imageninceptionv3).pack()

    btn4= Button(frame1, text='Limpiar', command=borrarPantalla,bg="dark gray").pack()

ejecutar()

raiz.mainloop()

