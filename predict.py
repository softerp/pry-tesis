
########################
import numpy as np
from keras.preprocessing.image import load_img, img_to_array

from keras.models import load_model
import h5py


from tensorflow.python.util.tf_export import keras_export



from keras.utils import CustomObjectScope
from keras.initializers import glorot_uniform
def predecir(ruta_img):

  longitud, altura = 150, 150
  modelo = './modelos/modelo-propio/modelo.h5'
  pesos_modelo = './modelos/modelo-propio/pesos.h5'

  
  with CustomObjectScope({'GlorotUniform': glorot_uniform()}):
    cnn = load_model(modelo)

  #cnn = load_model(modelo)
  cnn.load_weights(pesos_modelo)


  x = load_img(ruta_img, target_size=(longitud, altura))
  x = img_to_array(x)
  x = np.expand_dims(x, axis=0)
  array = cnn.predict(x)
  result = array[0]
  answer = np.argmax(result)
  Res=""
  if answer == 0:
    Res="Hemileia Vastatrix"
  elif answer == 1:
    Res="Leucoptera Coffella"
  elif answer == 2:
    Res="Mycena Citricolor"
  

  return answer, Res, result# indice, enfermedad, porcentaje de similitud


#predecir('./predicciones/IMG_1658.JPG')

#--------------------------------------------------
# CASO MODELO VGG16

from keras.preprocessing import image
  


def predecirVGG16(ruta_img):

  with CustomObjectScope({'GlorotUniform': glorot_uniform()}):
    cnn_VGG16 = load_model('./modelos/vgg16/vgg16_1.h5')

  x = load_img(ruta_img, target_size=(224, 224))
  x = img_to_array(x)
  x = np.expand_dims(x, axis=0)
  array = cnn_VGG16.predict(x)
  result = array[0]
  answer = np.argmax(result)
  Res=""
  if answer == 0:
    Res="Hemileia Vastatrix"
  elif answer == 1:
    Res="Leucoptera Coffella"
  elif answer == 2:
    Res="Mycena Citricolor"
  

  return answer, Res, result

def predecirAlexNet(ruta_img):# falt
  with CustomObjectScope({'GlorotUniform': glorot_uniform()}):
    cnn_VGG16 = load_model('./modelos/alexnet/alex.h5')

  x = load_img(ruta_img, target_size=(224, 224))
  x = img_to_array(x)
  x = np.expand_dims(x, axis=0)
  array = cnn_VGG16.predict(x)
  result = array[0]
  answer = np.argmax(result)
  Res=""
  if answer == 0:
    Res="Hemileia Vastatrix"
  elif answer == 1:
    Res="Leucoptera Coffella"
  elif answer == 2:
    Res="Mycena Citricolor"
  return answer, Res, result


def predecirInceptionV3(ruta_img):#falt

  longitud, altura = 150, 150
  modelo = './modelos/inceptionv3/modelo.h5'
  pesos_modelo = './modelos/inceptionv3/pesos.h5'

  
  with CustomObjectScope({'GlorotUniform': glorot_uniform()}):
    cnn = load_model(modelo)

  #cnn = load_model(modelo)
  cnn.load_weights(pesos_modelo)


  x = load_img(ruta_img, target_size=(longitud, altura))
  x = img_to_array(x)
  x = np.expand_dims(x, axis=0)
  array = cnn.predict(x)
  result = array[0]
  answer = np.argmax(result)
  Res=""
  if answer == 0:
    Res="Hemileia Vastatrix"
  elif answer == 1:
    Res="Leucoptera Coffella"
  elif answer == 2:
    Res="Mycena Citricolor"
  

  return answer, Res, result# indice, enfermedad, porcentaje de similitud