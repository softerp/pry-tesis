

#enfermedades biologia,agente,daño,control
Hemileia=['Manchas redondeadas amarillas naranjas, en el impacto se desprende un polvo, aumentan gradualemente',
'Hongo Hemileia Vastatrix',
'Defoliación y muerte descendente de acuerdo a la infestación. Caída de hojas',
'Aplicaciones de ocicloruro de cobre despues de la  floración,  Aplicar azufre si se presenta  sequía,  fertilización balanceada  con  calcio,aplicar  Bonomil,  Folicur y Stroby',
'Roya de Café'
]#roya

Leucoptera=['presencia de oviposición en el haz de las hojas durante la noche, la larva penetra enla dermis de la hoja, se presencia mayor infestación en sequía, favorecido por el exceso de sombra',
'polilla Perileucoptera coffeella',
'Minadura en aspecto de ampolla al iniciar verdoso claro y se transforma en un color pardo marrón oscuro',
'Evitar el abuso de abonos nitrogenados. Crianza y liberacíon de  parasitoides(Neochrysocharis  inmaculatus, Cirrospilus,Microlygus  y  Pediobius).  Crianza  y  liberaci ́on  de  preda-dores(Crematogaster  y  Chrisoperla)Control  qu ́ımico  conel  uso  de  insecticidas  traslaminantes  como  el  lufenur ́on  yabamecticina..  Abonamiento  rico  encalcio(fortalecimientode la planta. Evitar el exceso de sombra en el cultivo','Minador de Café']#minador

Mycena=['Manchas redondas un-didas, diferentes  tamaños,  colores  amarillos al início y pardo alfina',
'Hongo Mycena Citricolor','Emboscamiento de parcela, evitando el florecimiento proporcional',
'Realizar podas de ventilación,  raleo  del  bosque   aledaño,   aplicar Amistar y Folicur','Ojo de Pollo'
]#ojo de pollo


def Informacion_tecnica_por_enfermedad(enfermedad):
	if enfermedad=='Hemileia Vastatrix':
		return Hemileia
	if enfermedad=='Leucoptera Coffella':
		return Leucoptera
	if enfermedad=='Mycena Citricolor':
		return Mycena


def mostrar_nombre_comun(enfermedad):
	if enfermedad=='Hemileia Vastatrix':
		return Hemileia[4]
	if enfermedad=='Leucoptera Coffella':
		return Leucoptera[4]
	if enfermedad=='Mycena Citricolor':
		return Mycena[4]